module gitlab.com/tslocum/godoc-static

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/yuin/goldmark v1.1.23
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b
)
